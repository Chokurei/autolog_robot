#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 20 11:34:22 2018

@author: kaku
"""

import time
import pandas as pd
import numpy as np
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import schedule_generator

def clock_in_out(usernameStr, passwordStr, **kwargs):
    """
    clock in and out
    """
    browser = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
    browser.get(('https://ut-ppsweb.adm.u-tokyo.ac.jp/cws/srwtimerec'))
    
    username = browser.find_element_by_name('user_id')
    username.send_keys(usernameStr)
    
    # wait for transition then continue to fill items
    password = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.NAME, 'password')))
    password.send_keys(passwordStr)
    
    if kwargs['switch'] == 'in':
        signButton = browser.find_element_by_name('syussya')
    else:
        signButton = browser.find_element_by_name('taisya')
    signButton.click()

    countdown_timer()    
    browser.close()  
    
def countdown_timer():
    """
    time delayer
    """
    break_times = 3
    break_count = 0
    
    while break_count < break_times:
        time.sleep(5)
        break_count += 1

def sign_in_time(sign_in_file, now_object):
    sign_times = pd.read_csv(sign_in_file, index_col=0).values
    for idx in range(sign_times.shape[0]):
        sign_time = sign_times[idx]  
        deadline_arr = np.copy(sign_time)
        deadline_object = arr2datetime(deadline_arr)        
        sign_time[-2] = sign_time[-2] - 1
        sign_time[-1] = sign_time[-1] + np.int(np.random.normal(45, 3, 1))
        sign_time_object = arr2datetime(sign_time)
        if now_object == sign_time_object or now_object == deadline_object:
            clock_in_out(usernameStr, passwordStr, switch = 'in')
            print('Successfully log-in at:')
            print(now_object)
            print('Sleeping zzzzzz')
            time.sleep(23400)
            print('Waking up')
        
def sign_out_time(sign_out_file, now_object):
    sign_times = pd.read_csv(sign_out_file, index_col=0).values
    for idx in range(sign_times.shape[0]):
        sign_time = sign_times[idx]
        deadline_arr = np.copy(sign_time)
        deadline_arr[-1] += 30
        deadline_object = arr2datetime(deadline_arr)
        sign_time[-1] = sign_time[-1] + np.int(np.random.normal(15, 3, 1))
        sign_time_object = arr2datetime(sign_time)
        if now_object == sign_time_object or now_object == deadline_object:
            clock_in_out(usernameStr, passwordStr, switch = 'out')
            print('Successfully log-out at:')
            print(now_object)
            print('Sleeping zzzzzz')
            time.sleep(50400)
            print('Waking up')

def arr2datetime(arr):
    time_str = str(arr[0])+' '+str(arr[1])+' '+str(arr[2])+' '+str(arr[3])+' '+str(arr[4])
    datetime_object = datetime.strptime(time_str, '%Y %m %d %H %M')
    return datetime_object

def time_int_get(time):
    return int(time.strftime('%Y')), int(time.strftime('%m')), int(time.strftime('%d')), int(time.strftime('%H')), int(time.strftime('%M'))

usernameStr = '1622082260'
passwordStr = 'fF20070530?'
#initialize

now = datetime.now()
now_year, now_month, now_date, now_hour, now_min = time_int_get(now)

sign_in_file = '/media/kaku/Work/autoLogin/clock_in_%s_%s.csv'%(now_year,now_month)
sign_out_file = '/media/kaku/Work/autoLogin/clock_out_%s_%s.csv'%(now_year,now_month)
print('Hello Clock Timer')

while(1):
    now = datetime.now()
    now_year, now_month, now_date, now_hour, now_min = time_int_get(now)
    # schedule renew at 08:10 in new month
    if now_date == 1 and now_hour==8 and now_min == 10:
        schedule_generator.main()
        print('Happy new month, schedule is created')
        time.sleep(100)
        sign_in_file = '/media/kaku/Work/autoLogin/clock_in_{}_{}.csv'.format(str(now_year), str(now_month))
        sign_out_file = '/media/kaku/Work/autoLogin/clock_out_{}_{}.csv'.format(str(now_year), str(now_month))
    
    now_str = now.strftime('%Y %m %d %H %M')
    now_object = datetime.strptime(now_str, '%Y %m %d %H %M')
    time.sleep(10)
    sign_in_time(sign_in_file, now_object)
    sign_out_time(sign_out_file, now_object) 
    
            
