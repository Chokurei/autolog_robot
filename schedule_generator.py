#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 16:04:25 2018

@author: kaku

Automatically obtain working time form website table and make csv file
"""

import time
import pandas as pd
from datetime import datetime
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def countdown_timer():
    break_times = 3
    break_count = 0
    
    while break_count < break_times:
        time.sleep(2)
        break_count += 1

def get_in_website(usernameStr, passwordStr):
    """
    login and get table's website html
    """
    browser = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
    browser.get(('https://ut-ppsweb.adm.u-tokyo.ac.jp/cws/cws?isFromCWS=true&@SN=root.cws.cwslauncher'))
    
    username = browser.find_element_by_name('uid')
    username.send_keys(usernameStr)
    
    password = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.NAME, 'pwd')))
    password.send_keys(passwordStr)
    
    logInButton = browser.find_element_by_name('Login')
    logInButton.click()
    
    countdown_timer()  
    
    logInButton2 = browser.find_element_by_link_text('就労管理')
    logInButton2.click()
    
    countdown_timer()
    
    logInButton3 = browser.find_element_by_link_text('勤務表')
    logInButton3.click()
    
    countdown_timer()
    
    current_html = browser.page_source
    return current_html, browser

def get_clock_times(html):
    soup = BeautifulSoup(html,'html.parser')
    for tables in soup.findAll('table',{'class':'mg_outer'}):
        working_days = []
        for tr in tables.findAll('tr'):
            days = [td for td in tr.stripped_strings]
            if days[4] == '出勤日':
                working_days.append(days)
    
    clock_times = []
    useful_idx = [0, 2, 5, 7, 9, 11]    
    now = datetime.now()
    year = int(now.strftime('%Y'))
    
    for i in range(len(working_days)):
        clock_time = []
        working_day = working_days[i]
        clock_time.append(i+1)
        clock_time.append(year)
        for j in useful_idx:
            clock_time.append(int(working_day[j]))
        clock_times.append(clock_time)
    
    month = int(clock_time[2])
    return clock_times, year, month

def main():
    usernameStr = '1622082260'
    passwordStr = 'fF20070530?'    

    html, browser = get_in_website(usernameStr, passwordStr)
    clock_times, year, month = get_clock_times(html)
    
    columns = ['date', 'year', 'month', 'day', 'hour', 'min']
    
    clock_times_pd = pd.DataFrame(clock_times)
    clock_in_times = clock_times_pd.iloc[:,:6]
    clock_in_times.columns = columns 
    clock_in_times.to_csv('/media/kaku/Work/autoLogin/clock_in_{}_{}.csv'.format(str(year),str(month)), index = False)
    
    clock_out_times = clock_times_pd.iloc[:,[0,1,2,3,6,7]]
    clock_out_times.columns = columns 
    clock_out_times.to_csv('/media/kaku/Work/autoLogin/clock_out_{}_{}.csv'.format(str(year),str(month)), index = False)
    
    browser.close() 

if __name__ == '__main__':
    main()
    print('New schedule generated!')
    
    




